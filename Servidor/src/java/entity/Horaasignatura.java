/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kike
 */
@Entity
@Table(name = "horaasignatura")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Horaasignatura.findAll", query = "SELECT h FROM Horaasignatura h"),
    @NamedQuery(name = "Horaasignatura.findByIdHoraAsignatura", query = "SELECT h FROM Horaasignatura h WHERE h.idHoraAsignatura = :idHoraAsignatura"),
    @NamedQuery(name = "Horaasignatura.findByLetra", query = "SELECT h FROM Horaasignatura h WHERE h.letra = :letra"),
    @NamedQuery(name = "Horaasignatura.findByDia", query = "SELECT h FROM Horaasignatura h WHERE h.dia = :dia"),
    @NamedQuery(name = "Horaasignatura.findByHoraInicio", query = "SELECT h FROM Horaasignatura h WHERE h.horaInicio = :horaInicio"),
    @NamedQuery(name = "Horaasignatura.findByHoraFinal", query = "SELECT h FROM Horaasignatura h WHERE h.horaFinal = :horaFinal"),
    @NamedQuery(name = "Horaasignatura.findByAula", query = "SELECT h FROM Horaasignatura h WHERE h.aula = :aula"),
    @NamedQuery(name = "Horaasignatura.findByTipoHora", query = "SELECT h FROM Horaasignatura h WHERE h.tipoHora = :tipoHora")})
public class Horaasignatura implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idHoraAsignatura")
    private Integer idHoraAsignatura;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "letra")
    private String letra;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "dia")
    private String dia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "horaInicio")
    @Temporal(TemporalType.TIME)
    private Date horaInicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "horaFinal")
    @Temporal(TemporalType.TIME)
    private Date horaFinal;
    @Size(max = 45)
    @Column(name = "aula")
    private String aula;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "tipoHora")
    private String tipoHora;
    @JoinColumn(name = "idAsignatura", referencedColumnName = "idAsignatura")
    @ManyToOne(optional = false)
    private Asignatura idAsignatura;

    public Horaasignatura() {
    }

    public Horaasignatura(Integer idHoraAsignatura) {
        this.idHoraAsignatura = idHoraAsignatura;
    }

    public Horaasignatura(Integer idHoraAsignatura, String letra, String dia, Date horaInicio, Date horaFinal, String tipoHora) {
        this.idHoraAsignatura = idHoraAsignatura;
        this.letra = letra;
        this.dia = dia;
        this.horaInicio = horaInicio;
        this.horaFinal = horaFinal;
        this.tipoHora = tipoHora;
    }

    public Integer getIdHoraAsignatura() {
        return idHoraAsignatura;
    }

    public void setIdHoraAsignatura(Integer idHoraAsignatura) {
        this.idHoraAsignatura = idHoraAsignatura;
    }

    public String getLetra() {
        return letra;
    }

    public void setLetra(String letra) {
        this.letra = letra;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public Date getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio) {
        this.horaInicio = horaInicio;
    }

    public Date getHoraFinal() {
        return horaFinal;
    }

    public void setHoraFinal(Date horaFinal) {
        this.horaFinal = horaFinal;
    }

    public String getAula() {
        return aula;
    }

    public void setAula(String aula) {
        this.aula = aula;
    }

    public String getTipoHora() {
        return tipoHora;
    }

    public void setTipoHora(String tipoHora) {
        this.tipoHora = tipoHora;
    }

    public Asignatura getIdAsignatura() {
        return idAsignatura;
    }

    public void setIdAsignatura(Asignatura idAsignatura) {
        this.idAsignatura = idAsignatura;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idHoraAsignatura != null ? idHoraAsignatura.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Horaasignatura)) {
            return false;
        }
        Horaasignatura other = (Horaasignatura) object;
        if ((this.idHoraAsignatura == null && other.idHoraAsignatura != null) || (this.idHoraAsignatura != null && !this.idHoraAsignatura.equals(other.idHoraAsignatura))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Horaasignatura[ idHoraAsignatura=" + idHoraAsignatura + " ]";
    }
    
}
