/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kike
 */
@Entity
@Table(name = "actualizacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Actualizacion.findAll", query = "SELECT a FROM Actualizacion a"),
    @NamedQuery(name = "Actualizacion.findByIdActualizacion", query = "SELECT a FROM Actualizacion a WHERE a.idActualizacion = :idActualizacion"),
    @NamedQuery(name = "Actualizacion.findByUltimaActualizacion", query = "SELECT a FROM Actualizacion a WHERE a.ultimaActualizacion = :ultimaActualizacion")})
public class Actualizacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idActualizacion")
    private Integer idActualizacion;
    @Column(name = "ultimaActualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ultimaActualizacion;

    public Actualizacion() {
    }

    public Actualizacion(Integer idActualizacion) {
        this.idActualizacion = idActualizacion;
    }

    public Integer getIdActualizacion() {
        return idActualizacion;
    }

    public void setIdActualizacion(Integer idActualizacion) {
        this.idActualizacion = idActualizacion;
    }

    public Date getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    public void setUltimaActualizacion(Date ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idActualizacion != null ? idActualizacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Actualizacion)) {
            return false;
        }
        Actualizacion other = (Actualizacion) object;
        if ((this.idActualizacion == null && other.idActualizacion != null) || (this.idActualizacion != null && !this.idActualizacion.equals(other.idActualizacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Actualizacion[ idActualizacion=" + idActualizacion + " ]";
    }
    
}
