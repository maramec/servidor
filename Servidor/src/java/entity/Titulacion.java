/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kike
 */
@Entity
@Table(name = "titulacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Titulacion.findAll", query = "SELECT t FROM Titulacion t"),
    @NamedQuery(name = "Titulacion.findByIdTitulacion", query = "SELECT t FROM Titulacion t WHERE t.idTitulacion = :idTitulacion"),
    @NamedQuery(name = "Titulacion.findByNombreTitulacion", query = "SELECT t FROM Titulacion t WHERE t.nombreTitulacion = :nombreTitulacion")})
public class Titulacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idTitulacion")
    private Integer idTitulacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombreTitulacion")
    private String nombreTitulacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "titulacionidTitulacion")
    private Collection<Alumno> alumnoCollection;

    public Titulacion() {
    }

    public Titulacion(Integer idTitulacion) {
        this.idTitulacion = idTitulacion;
    }

    public Titulacion(Integer idTitulacion, String nombreTitulacion) {
        this.idTitulacion = idTitulacion;
        this.nombreTitulacion = nombreTitulacion;
    }

    public Integer getIdTitulacion() {
        return idTitulacion;
    }

    public void setIdTitulacion(Integer idTitulacion) {
        this.idTitulacion = idTitulacion;
    }

    public String getNombreTitulacion() {
        return nombreTitulacion;
    }

    public void setNombreTitulacion(String nombreTitulacion) {
        this.nombreTitulacion = nombreTitulacion;
    }

    @XmlTransient
    public Collection<Alumno> getAlumnoCollection() {
        return alumnoCollection;
    }

    public void setAlumnoCollection(Collection<Alumno> alumnoCollection) {
        this.alumnoCollection = alumnoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTitulacion != null ? idTitulacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Titulacion)) {
            return false;
        }
        Titulacion other = (Titulacion) object;
        if ((this.idTitulacion == null && other.idTitulacion != null) || (this.idTitulacion != null && !this.idTitulacion.equals(other.idTitulacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Titulacion[ idTitulacion=" + idTitulacion + " ]";
    }
    
}
