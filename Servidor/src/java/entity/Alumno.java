/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kike
 */
@Entity
@Table(name = "alumno")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Alumno.findAll", query = "SELECT a FROM Alumno a"),
    @NamedQuery(name = "Alumno.findByNumExpediente", query = "SELECT a FROM Alumno a WHERE a.numExpediente = :numExpediente"),
    @NamedQuery(name = "Alumno.findByNombre", query = "SELECT a FROM Alumno a WHERE a.nombre = :nombre"),
    @NamedQuery(name = "Alumno.findByApellidos", query = "SELECT a FROM Alumno a WHERE a.apellidos = :apellidos"),
    @NamedQuery(name = "Alumno.findByCorreoExterno", query = "SELECT a FROM Alumno a WHERE a.correoExterno = :correoExterno")})
public class Alumno implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "numExpediente")
    private Integer numExpediente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "apellidos")
    private String apellidos;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "correoExterno")
    private String correoExterno;
    @JoinTable(name = "alumnoasignatura", joinColumns = {
        @JoinColumn(name = "alumno_numExpediente", referencedColumnName = "numExpediente")}, inverseJoinColumns = {
        @JoinColumn(name = "asignatura_idAsignatura", referencedColumnName = "idAsignatura")})
    @ManyToMany
    private Collection<Asignatura> asignaturaCollection;
    @JoinColumn(name = "titulacion_idTitulacion", referencedColumnName = "idTitulacion")
    @ManyToOne(optional = false)
    private Titulacion titulacionidTitulacion;

    public Alumno() {
    }

    public Alumno(Integer numExpediente) {
        this.numExpediente = numExpediente;
    }

    public Alumno(Integer numExpediente, String nombre, String apellidos, String correoExterno) {
        this.numExpediente = numExpediente;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.correoExterno = correoExterno;
    }

    public Integer getNumExpediente() {
        return numExpediente;
    }

    public void setNumExpediente(Integer numExpediente) {
        this.numExpediente = numExpediente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreoExterno() {
        return correoExterno;
    }

    public void setCorreoExterno(String correoExterno) {
        this.correoExterno = correoExterno;
    }

    @XmlTransient
    public Collection<Asignatura> getAsignaturaCollection() {
        return asignaturaCollection;
    }

    public void setAsignaturaCollection(Collection<Asignatura> asignaturaCollection) {
        this.asignaturaCollection = asignaturaCollection;
    }

    public Titulacion getTitulacionidTitulacion() {
        return titulacionidTitulacion;
    }

    public void setTitulacionidTitulacion(Titulacion titulacionidTitulacion) {
        this.titulacionidTitulacion = titulacionidTitulacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numExpediente != null ? numExpediente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alumno)) {
            return false;
        }
        Alumno other = (Alumno) object;
        if ((this.numExpediente == null && other.numExpediente != null) || (this.numExpediente != null && !this.numExpediente.equals(other.numExpediente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Alumno[ numExpediente=" + numExpediente + " ]";
    }
    
}
