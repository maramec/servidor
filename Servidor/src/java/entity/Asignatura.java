/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kike
 */
@Entity
@Table(name = "asignatura")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Asignatura.findAll", query = "SELECT a FROM Asignatura a"),
    @NamedQuery(name = "Asignatura.findByIdAsignatura", query = "SELECT a FROM Asignatura a WHERE a.idAsignatura = :idAsignatura"),
    @NamedQuery(name = "Asignatura.findByNombreAsignatura", query = "SELECT a FROM Asignatura a WHERE a.nombreAsignatura = :nombreAsignatura"),
    @NamedQuery(name = "Asignatura.findByFechaExamen", query = "SELECT a FROM Asignatura a WHERE a.fechaExamen = :fechaExamen"),
    @NamedQuery(name = "Asignatura.findByHoraExamen", query = "SELECT a FROM Asignatura a WHERE a.horaExamen = :horaExamen"),
    @NamedQuery(name = "Asignatura.findByAulaExamen", query = "SELECT a FROM Asignatura a WHERE a.aulaExamen = :aulaExamen"),
    @NamedQuery(name = "Asignatura.findByOptativa", query = "SELECT a FROM Asignatura a WHERE a.optativa = :optativa"),
    @NamedQuery(name = "Asignatura.findBySemestre", query = "SELECT a FROM Asignatura a WHERE a.semestre = :semestre")})
public class Asignatura implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idAsignatura")
    private Integer idAsignatura;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombreAsignatura")
    private String nombreAsignatura;
    @Column(name = "fechaExamen")
    @Temporal(TemporalType.DATE)
    private Date fechaExamen;
    @Column(name = "horaExamen")
    @Temporal(TemporalType.TIME)
    private Date horaExamen;
    @Size(max = 45)
    @Column(name = "aulaExamen")
    private String aulaExamen;
    @Basic(optional = false)
    @NotNull
    @Column(name = "optativa")
    private boolean optativa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "semestre")
    private int semestre;
    @ManyToMany(mappedBy = "asignaturaCollection")
    private Collection<Profesor> profesorCollection;
    @ManyToMany(mappedBy = "asignaturaCollection")
    private Collection<Alumno> alumnoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAsignatura")
    private Collection<Horaasignatura> horaasignaturaCollection;

    public Asignatura() {
    }

    public Asignatura(Integer idAsignatura) {
        this.idAsignatura = idAsignatura;
    }

    public Asignatura(Integer idAsignatura, String nombreAsignatura, boolean optativa, int semestre) {
        this.idAsignatura = idAsignatura;
        this.nombreAsignatura = nombreAsignatura;
        this.optativa = optativa;
        this.semestre = semestre;
    }

    public Integer getIdAsignatura() {
        return idAsignatura;
    }

    public void setIdAsignatura(Integer idAsignatura) {
        this.idAsignatura = idAsignatura;
    }

    public String getNombreAsignatura() {
        return nombreAsignatura;
    }

    public void setNombreAsignatura(String nombreAsignatura) {
        this.nombreAsignatura = nombreAsignatura;
    }

    public Date getFechaExamen() {
        return fechaExamen;
    }

    public void setFechaExamen(Date fechaExamen) {
        this.fechaExamen = fechaExamen;
    }

    public Date getHoraExamen() {
        return horaExamen;
    }

    public void setHoraExamen(Date horaExamen) {
        this.horaExamen = horaExamen;
    }

    public String getAulaExamen() {
        return aulaExamen;
    }

    public void setAulaExamen(String aulaExamen) {
        this.aulaExamen = aulaExamen;
    }

    public boolean getOptativa() {
        return optativa;
    }

    public void setOptativa(boolean optativa) {
        this.optativa = optativa;
    }

    public int getSemestre() {
        return semestre;
    }

    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }

    @XmlTransient
    public Collection<Profesor> getProfesorCollection() {
        return profesorCollection;
    }

    public void setProfesorCollection(Collection<Profesor> profesorCollection) {
        this.profesorCollection = profesorCollection;
    }

    @XmlTransient
    public Collection<Alumno> getAlumnoCollection() {
        return alumnoCollection;
    }

    public void setAlumnoCollection(Collection<Alumno> alumnoCollection) {
        this.alumnoCollection = alumnoCollection;
    }

    @XmlTransient
    public Collection<Horaasignatura> getHoraasignaturaCollection() {
        return horaasignaturaCollection;
    }

    public void setHoraasignaturaCollection(Collection<Horaasignatura> horaasignaturaCollection) {
        this.horaasignaturaCollection = horaasignaturaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAsignatura != null ? idAsignatura.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Asignatura)) {
            return false;
        }
        Asignatura other = (Asignatura) object;
        if ((this.idAsignatura == null && other.idAsignatura != null) || (this.idAsignatura != null && !this.idAsignatura.equals(other.idAsignatura))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Asignatura[ idAsignatura=" + idAsignatura + " ]";
    }
    
}
