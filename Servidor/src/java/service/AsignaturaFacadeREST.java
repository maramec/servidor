/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package service;

import dao.AlumnoFacade;
import dao.AsignaturaFacade;
import dao.ProfesorFacade;
import entity.Alumno;
import entity.Asignatura;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author Kike
 */
@Stateless
@Path("entity.asignatura")
public class AsignaturaFacadeREST extends AbstractFacade<Asignatura> {
    @EJB
    private ProfesorFacade profesorFacade;
    @EJB
    private AlumnoFacade alumnoFacade;
    @PersistenceContext(unitName = "ServidorPU")
    private EntityManager em;
    

    public AsignaturaFacadeREST() {
        super(Asignatura.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Asignatura entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Asignatura entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public Asignatura find(@PathParam("id") Integer id) {
        return super.find(id);
    }
    
    @GET
    @Path("getDeAlumno/{id}")
    @Produces({"aplication/json"})
    public List<Asignatura> getAsignaturasAlumno(@PathParam("id") Integer id){
        List<Asignatura> todas = new ArrayList(super.findAll());
        List<Asignatura> matriculadas = new ArrayList();
        for(Asignatura asignatura:todas){
            List<Asignatura> asignaturasAlumno = new ArrayList();
            asignaturasAlumno = (List) alumnoFacade.find(id).getAsignaturaCollection();
            if(asignaturasAlumno.contains(asignatura)){
                matriculadas.add(asignatura);
            }
        }
        return matriculadas;
    }
    
    @GET
    @Path("getDeProfesor/{id}")
    @Produces({"aplication/json"})
    public List<Asignatura> getAsignaturasProfesor(@PathParam("id") Integer id){
        List<Asignatura> todas = new ArrayList(super.findAll());
        List<Asignatura> impartidas = new ArrayList();
        for(Asignatura asignatura:todas){
            List<Asignatura> asignaturasProfesor = new ArrayList();
            asignaturasProfesor = (List) profesorFacade.find(id).getAsignaturaCollection();
            if(asignaturasProfesor.contains(asignatura)){
                impartidas.add(asignatura);
            }
        }
        return impartidas;
    }

    @GET
    @Override
    @Produces({"application/json"})
    public List<Asignatura> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Asignatura> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
