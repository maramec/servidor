/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package servlet;

import com.csvreader.CsvReader;
import dao.AlumnoFacade;
import dao.AsignaturaFacade;
import dao.HoraasignaturaFacade;
import dao.ProfesorFacade;
import dao.TitulacionFacade;
import entity.Alumno;
import entity.Asignatura;
import entity.Horaasignatura;
import entity.Profesor;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Kike
 */
@WebServlet(name = "MyServlet", urlPatterns = {"/MyServlet"}, loadOnStartup = 1)
public class MyServlet extends HttpServlet {
    @EJB
    private HoraasignaturaFacade horaasignaturaFacade;
    @EJB
    private ProfesorFacade profesorFacade;
    @EJB
    private AlumnoFacade alumnoFacade;
    @EJB
    private AsignaturaFacade asignaturaFacade;
    @EJB
    private TitulacionFacade titulacionFacade;
    
    @Override
    public void init(){
        try{
            System.out.println("*******************Main*******************");
            Properties prop = new Properties();
            System.out.println(this.getServletContext().getRealPath(""));
            FileReader reader = new FileReader(this.getServletContext().getRealPath("")+"\\WEB-INF\\MyProperties.properties");
            prop.load(reader);
            String alum = prop.getProperty("alumnos");
            String asig = prop.getProperty("asignaturas");
            String prof = prop.getProperty("profesores");
            String horas = prop.getProperty("horasasignatura");
            insertarProfesores(prof);
            insertarAsignaturas(asig);
            insertarAlumnos(alum);
            insertarHoraAsignatura(horas);
        }catch(FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @SuppressWarnings("CallToPrintStackTrace")
    private void insertarProfesores(String s){
        try {         
            System.out.println("*******************Insertando Profesores*******************");
            CsvReader profesores_import = new CsvReader(s);
            profesores_import.readHeaders();
            
            while (profesores_import.readRecord())
            {
                int idp = Integer.parseInt(profesores_import.get(0));
                String nombre = profesores_import.get(1);
                String apellidos = profesores_import.get(2);
                int telefono = Integer.parseInt(profesores_import.get(3));
                String correo = profesores_import.get(4);
                String despacho = profesores_import.get(5);
                String tutorias = profesores_import.get(6);
                
                if(existProfesor(idp)==0){
                    Profesor pr = new Profesor(idp);
                    pr.setNombre(nombre);
                    pr.setApellidos(apellidos);
                    pr.setTelefono(telefono);
                    pr.setEmail(correo);
                    pr.setDespacho(despacho);
                    pr.setHorarioTutorias(tutorias);
                    List<Asignatura> l = new ArrayList();
                    pr.setAsignaturaCollection(l);
                    profesorFacade.create(pr);
                }
            }
            profesores_import.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @SuppressWarnings("CallToPrintStackTrace")
    private void insertarAsignaturas(String s){
        try {         
            System.out.println("*******************Insertando Asignaturas*******************");
            CsvReader asignaturas_import = new CsvReader(s);
            asignaturas_import.readHeaders();
            
            while (asignaturas_import.readRecord())
            {
                int codigo = Integer.parseInt(asignaturas_import.get(0));
                String nombre = asignaturas_import.get(1);
                Date fechaExamen = Date.valueOf(asignaturas_import.get(2));
                Time horaExamen = Time.valueOf(asignaturas_import.get(3));
                String aula = asignaturas_import.get(4);
                boolean optativa = Boolean.getBoolean(asignaturas_import.get(5));
                int semestre = Integer.parseInt(asignaturas_import.get(6));
                int profesor = Integer.parseInt(asignaturas_import.get(7));

                if(asignaturaFacade.find(codigo)==null){
                    Asignatura as = new Asignatura(codigo, nombre, optativa, semestre);
                    as.setFechaExamen(fechaExamen);
                    as.setHoraExamen(horaExamen);
                    as.setAulaExamen(aula);
                    List<Profesor> l = new ArrayList();
                    l.add(profesorFacade.find(profesor));
                    as.setProfesorCollection(l);
                    Profesor prof = profesorFacade.find(profesor);
                    List<Asignatura> laux = new ArrayList(prof.getAsignaturaCollection());
                    if (!laux.contains(as)){
                        laux.add(as);
                        prof.setAsignaturaCollection(laux);
                        profesorFacade.edit(prof);
                    }
                    System.out.println("--------Alert---------");
                    //asignaturaFacade.create(as);
                }
            }
            asignaturas_import.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @SuppressWarnings("CallToPrintStackTrace")
    private void insertarAlumnos(String s){
        try {         
            System.out.println("*******************Insertando Alumnos*******************");
            CsvReader alumnos_import = new CsvReader(s);
            alumnos_import.readHeaders();
            
            while (alumnos_import.readRecord())
            {
                int expediente = Integer.parseInt(alumnos_import.get(0));
                String nombre = alumnos_import.get(1);
                String apellidos = alumnos_import.get(2);
                String correo = alumnos_import.get(3);
                int titulacion = Integer.parseInt(alumnos_import.get(4));
                int asignatura = Integer.parseInt(alumnos_import.get(5));

                if(alumnoFacade.find(expediente)!=null){
                    Alumno alu = alumnoFacade.find(expediente);
                    alu.setApellidos(apellidos);
                    alu.setCorreoExterno(correo);
                    alu.setNombre(nombre);
                    alu.setTitulacionidTitulacion(titulacionFacade.find(titulacion));
                    alu.setNumExpediente(expediente);
                    List<Asignatura> li = new ArrayList(alu.getAsignaturaCollection());
                    if(!li.contains(asignaturaFacade.find(asignatura))){
                        li.add(asignaturaFacade.find(asignatura));
                        alu.setAsignaturaCollection(li);
                    }
                    alumnoFacade.edit(alu);
                }else{
                    Alumno al = new Alumno(expediente, nombre, apellidos, correo);
                    List<Asignatura> l = new ArrayList();
                    l.add(asignaturaFacade.find(asignatura));
                    al.setAsignaturaCollection(l);
                    al.setTitulacionidTitulacion(titulacionFacade.find(titulacion));
                    alumnoFacade.create(al);
                }
            }
            alumnos_import.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void insertarHoraAsignatura(String s){
        try {         
            System.out.println("*******************Insertando Horas de Asignaturas*******************");
            StringTokenizer str = new StringTokenizer(s, "\\");
            String letra="";
            while (str.hasMoreElements()){
                if(!letra.equals("java")){
                    letra=str.nextToken();
                }else{
                    letra=str.nextToken();
                    StringTokenizer stk = new StringTokenizer(letra, ".");
                    letra=stk.nextToken();
                }
            }
            letra=letra.substring(1);
            CsvReader horasasignaturas_import = new CsvReader(s);
            horasasignaturas_import.readHeaders();
            int i=0;
            String aula="";
            
            while (horasasignaturas_import.readRecord())
            {
                i++;
                if(i==1){
                    aula = horasasignaturas_import.get(3);
                    aula=aula.substring(6);
                }else if(((i>1)&&(i<5))||((i>7)&&(i<11))){
                    String hora = horasasignaturas_import.get("Hora");
                    
                    String hlunes = horasasignaturas_import.get("Lunes");
                    Horaasignatura horalunes = new Horaasignatura();
                    horalunes.setAula(aula);
                    horalunes.setDia("Lunes");
                    horalunes.setHoraInicio(getHoraInicio(hora));
                    horalunes.setHoraFinal(getHoraFinal(hora));
                    Asignatura auxlu = getAsignatura(hlunes);
                    horalunes.setIdAsignatura(auxlu);
                    horalunes.setLetra(letra);
                    horalunes.setTipoHora("Obligatoria");
                    if((!existHoraAsignatura(horalunes))&&(!hlunes.equals("Opt. "))){
                        horaasignaturaFacade.create(horalunes);
                    }
                    
                    String hmartes = horasasignaturas_import.get("Martes");
                    Horaasignatura horamartes = new Horaasignatura();
                    horamartes.setAula(aula);
                    horamartes.setDia("Martes");
                    horamartes.setHoraInicio(getHoraInicio(hora));
                    horamartes.setHoraFinal(getHoraFinal(hora));
                    Asignatura auxma = getAsignatura(hmartes);
                    horamartes.setIdAsignatura(auxma);
                    horamartes.setLetra(letra);
                    horamartes.setTipoHora("Obligatoria");
                    if((!existHoraAsignatura(horamartes))&&(!hlunes.equals("Opt. "))){
                        horaasignaturaFacade.create(horamartes);
                    }
                    String hmiercoles = horasasignaturas_import.get("Miércoles");
                    Horaasignatura horamiercoles = new Horaasignatura();
                    horamiercoles.setAula(aula);
                    horamiercoles.setDia("Miércoles");
                    horamiercoles.setHoraInicio(getHoraInicio(hora));
                    horamiercoles.setHoraFinal(getHoraFinal(hora));
                    Asignatura auxmi = getAsignatura(hmartes);
                    horamiercoles.setIdAsignatura(auxmi);
                    horamiercoles.setLetra(letra);
                    horamiercoles.setTipoHora("Obligatoria");
                    if((!existHoraAsignatura(horamiercoles))&&(!hlunes.equals("Opt. "))){
                        horaasignaturaFacade.create(horamiercoles);
                    }
                    
                    String hjueves = horasasignaturas_import.get("Jueves");
                    Horaasignatura horajueves = new Horaasignatura();
                    horajueves.setAula(aula);
                    horajueves.setDia("Jueves");
                    horajueves.setHoraInicio(getHoraInicio(hora));
                    horajueves.setHoraFinal(getHoraFinal(hora));
                    Asignatura auxju = getAsignatura(hjueves);
                    horajueves.setIdAsignatura(auxju);
                    horajueves.setLetra(letra);
                    horajueves.setTipoHora("Obligatoria");
                    if((!existHoraAsignatura(horajueves))&&(!hlunes.equals("Opt. "))){
                        horaasignaturaFacade.create(horajueves);
                    }
                    
                    String hviernes = horasasignaturas_import.get("Viernes");
                    Horaasignatura horaviernes = new Horaasignatura();
                    horaviernes.setAula(aula);
                    horaviernes.setDia("Viernes");
                    horaviernes.setHoraInicio(getHoraInicio(hora));
                    horaviernes.setHoraFinal(getHoraFinal(hora));
                    Asignatura auxvi = getAsignatura(hviernes);
                    horaviernes.setIdAsignatura(auxvi);
                    horaviernes.setLetra(letra);
                    horaviernes.setTipoHora("Obligatoria");
                    if((!existHoraAsignatura(horaviernes))&&(!hlunes.equals("Opt. "))){
                        horaasignaturaFacade.create(horaviernes);
                    }
                }
            }
            horasasignaturas_import.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private boolean existHoraAsignatura(Horaasignatura ha){
        List<Horaasignatura> lauxha = new ArrayList(horaasignaturaFacade.findAll());
        for(Horaasignatura x:lauxha){
            if((x.getLetra().equalsIgnoreCase(ha.getLetra()))&&
                    (x.getDia().equalsIgnoreCase(ha.getDia()))&&
                    (x.getAula().equalsIgnoreCase(ha.getAula())&&
                    (x.getHoraFinal().equals(ha.getHoraFinal())))&&
                    (x.getHoraInicio().equals(ha.getHoraInicio()))&&
                    (x.getIdAsignatura().getIdAsignatura()==ha.getIdAsignatura().getIdAsignatura())&&
                    (x.getTipoHora().equalsIgnoreCase(ha.getTipoHora()))){
                return true;
            }
        }
        return false;
    }
    
    private int existProfesor(int m){
        List<Profesor> l = new ArrayList(profesorFacade.findAll());
        if(!l.isEmpty()){
            for (Profesor p : l){
                if(p.getIdProfesor()==m){
                    return p.getIdProfesor();
                }
            }
        }
        return 0;
    }
    
    private Asignatura getAsignatura(String s){
        List<Asignatura> li = new ArrayList(asignaturaFacade.findAll());
        for (Asignatura a:li){
            if (a.getNombreAsignatura().equalsIgnoreCase(s)){
                return asignaturaFacade.find(a.getIdAsignatura());
            }
        }
        return null;
    }
    
    private Time getHoraInicio(String s){
        int i = s.indexOf(" ");
        s=s.substring(0, i);
        s=s.concat(":00");
        return Time.valueOf(s);
    }
    
    private Time getHoraFinal(String s){
        int i = s.indexOf("a");
        s=s.substring(i+2);
        s=s.concat(":00");
        return Time.valueOf(s);
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MyServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MyServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}