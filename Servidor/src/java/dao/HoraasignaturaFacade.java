/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dao;

import entity.Horaasignatura;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Kike
 */
@Stateless
public class HoraasignaturaFacade extends AbstractFacade<Horaasignatura> {
    @PersistenceContext(unitName = "ServidorPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HoraasignaturaFacade() {
        super(Horaasignatura.class);
    }
    
}
