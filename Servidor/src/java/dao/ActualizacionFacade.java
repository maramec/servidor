/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dao;

import entity.Actualizacion;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Kike
 */
@Stateless
public class ActualizacionFacade extends AbstractFacade<Actualizacion> {
    @PersistenceContext(unitName = "ServidorPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ActualizacionFacade() {
        super(Actualizacion.class);
    }
    
}
