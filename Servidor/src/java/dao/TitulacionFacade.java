/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dao;

import entity.Titulacion;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Kike
 */
@Stateless
public class TitulacionFacade extends AbstractFacade<Titulacion> {
    @PersistenceContext(unitName = "ServidorPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TitulacionFacade() {
        super(Titulacion.class);
    }
    
}
